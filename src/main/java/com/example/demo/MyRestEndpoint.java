package com.example.demo;

import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MyRestEndpoint {

  @GetMapping(path = "/test")
  public String myEndPoint() {
    return "hello world " + LocalDateTime.now();
  }
}
