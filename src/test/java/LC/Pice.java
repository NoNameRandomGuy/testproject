package LC;

import java.util.List;
import org.junit.jupiter.api.Test;

class Pice {

  @Test
  void contextLoads() {
    int[] x0 = {7,1,5,3,6,4};
    int[] x1 = {1,2,3,4,5};
    int[] x2 = {7,6,4,3,1};
    int[] x3 = {};
    System.out.println(myMaxProfit(x0));
    System.out.println(myMaxProfit(x1));
    System.out.println(myMaxProfit(x2));
    System.out.println(myMaxProfit(x3));

  }

  public int myMaxProfit(int[] prices) {
    int profit = 0;

    for (int i = 1; i < prices.length; i ++) {
      if (prices[i] > prices[i-1]){
        profit += prices[i] - prices[i-1];
      }
    }
    return profit;
  }




















    public int maxProfit(int[] prices) {
    int i = 0;
    int valley = prices[0];
    int peak = prices[0];
    int maxprofit = 0;
    while (i < prices.length - 1) {
      while (i < prices.length - 1 && prices[i] >= prices[i + 1]) i++;
      valley = prices[i];
      while (i < prices.length - 1 && prices[i] <= prices[i + 1]) i++;
      peak = prices[i];
      maxprofit += peak - valley;
    }
    return maxprofit;
  }

  public int maxProfit2(int[] prices) {
    int maxprofit = 0;
    for (int i = 1; i < prices.length; i++) {
      if (prices[i] > prices[i - 1])
        maxprofit += prices[i] - prices[i - 1];
    }
    return maxprofit;
  }
}
