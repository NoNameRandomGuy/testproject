package LC;

import org.junit.jupiter.api.Test;

class Single_Number {

  @Test
  void test() {
    int[] ints = {2, 2, 1};
    int[] ints2 = {4, 1, 2, 1, 2, 2};

    System.out.println(find(ints));
    System.out.println(find(ints2));
  }

  private int find(int[] arr) {
    int res = 0;
    for (int j : arr) {
      res ^= j;
    }
    return res;
  }
}
