package LC;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;

class RemoveIlends {

  @Test
  void test() {

    List<List<Integer>> matrix = new ArrayList<>();
    matrix.add(new ArrayList<>(List.of(1, 0, 0, 0, 0, 0)));
    matrix.add(new ArrayList<>(List.of(0, 1, 0, 1, 1, 1)));
    matrix.add(new ArrayList<>(List.of(0, 0, 1, 0, 1, 0)));
    matrix.add(new ArrayList<>(List.of(1, 1, 0, 0, 1, 0)));
    matrix.add(new ArrayList<>(List.of(1, 0, 1, 1, 0, 0)));
    matrix.add(new ArrayList<>(List.of(1, 0, 0, 0, 0, 1)));

    print(matrix);
    doStuff(matrix);
    print(matrix);
  }

  public List<List<Integer>> doStuff(List<List<Integer>> matrix) {
    MatrixProcessor matrixProcessor = new MatrixProcessor(matrix.size(), matrix.get(0).size(), 1);


    for (int i = 0; i < matrix.size(); i++) {
      for (int j = 0; j < matrix.get(i).size(); j++) {
        if (matrixProcessor.isOk(i, j)) {

          Integer integer = matrix.get(i).get(j);
          if (integer == 1) {
            matrix.get(i).set(j, 0);
          }
        }
      }
    }
    return matrix;
  }


  public void print(List<List<Integer>> matrix) {
    for (List<Integer> list : matrix) {

      System.out.println();
      for (Integer i : list) {
        System.out.print(i + "  ");
      }
    }
    System.out.println();
  }

  public List<List<Integer>> doStuff1(List<List<Integer>> matrix) {
    MatrixProcessor matrixProcessor = new MatrixProcessor(matrix.size(), matrix.get(0).size(), 1);
    for (int i = 0; i < matrix.size(); i++) {
      for (int j = 0; j < matrix.get(i).size(); j++) {
        if (matrixProcessor.isOk(i, j)) {

          Integer integer = matrix.get(i).get(j);
          if (integer == 1) {
            matrix.get(i).set(j, 0);
          }
        }
      }
    }
    return matrix;
  }

  static class MatrixProcessor {

    private final int sizeY;
    private final int sizeX;
    private final int offset;

    public MatrixProcessor(int sizeY, int sizeX, int offset) {
      this.sizeY = sizeY;
      this.sizeX = sizeX;
      this.offset = offset;
    }

    public boolean isOk(int x, int y) {
      boolean isXok = 0 < x && x < sizeX - 1 - offset;
      boolean isYok = 0 < y && y < sizeY - 1 - offset;
      return isXok && isYok;
    }
  }
}
