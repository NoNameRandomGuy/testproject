package LC;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class SingleNumber {

  @Test
  void test() {

    assertEquals(singleNumber(new int[] {2,2,1}), 1);
    assertEquals(singleNumber(new int[] {4,1,2,1,2}), 4);
    assertEquals(singleNumber(new int[] {1}), 1);
  }

  public int singleNumber(int[] nums) {
    int a = 0;
    for (int i : nums) {
      a ^= i;
    }
    return a;
  }
}
