package LC;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Test;
import scala.Int;

class ReverseLinkList {

  @Test
  void test() {
    LinkedList<Integer> list = new LinkedList<>(Arrays.asList(1, 2, 4, 5, 6,7));
    print(list);
    reverseLinkList(list);
    print(list);
    //        assertArrayEquals(reverseLinkList(list), new LinkedList<Integer>(Arrays.asList(1, 2,
    // 4, 5, 6)));
    //    assertEquals(singleNumber(new int[] {4,1,2,1,2}), 4);
    //    assertEquals(singleNumber(new int[] {1}), 1);
  }

  public LinkedList<Integer> reverseLinkList(LinkedList<Integer> list) {
    for (int i = 0; i < list.size() / 2; i++) {
      Integer temp = list.get(i);
      int endIndex = list.size() - 1 - i;
      list.set(i, list.get(endIndex));
      list.set(endIndex, temp);
    }

    return list;
  }

  public void print(List<Integer> list) {
    for (int i : list) {
      System.out.print(i + " ");
    }
    System.out.println();
  }
}
