package LC;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ContainsDuplicate {

  @Test
  void test() {

    assertEquals(containsDuplicate(new int[] {1,2,3,1}), true);
    assertEquals(containsDuplicate(new int[] {1,2,3,4}), false);
    assertEquals(containsDuplicate(new int[] {1,1,1,3,3,4,3,2,4,2}), true);
  }

  public boolean containsDuplicate(int[] nums) {
    Set<Integer> set = new HashSet<>();
    for (int i : nums) {
      if (set.contains(i)){
        return true;
      }else {
        set.add(i);
      }
    }
    return false;
  }
}
