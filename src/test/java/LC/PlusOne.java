package LC;

import org.junit.jupiter.api.Test;

class PlusOne {

  @Test
  void test() {

    int[] nums1 = {4, 9, 5, 9};
    print(increment(nums1));
  }

  private int[] increment(int[] arr1) {
    int count = 0;
    boolean enoth = true;
    do {
      int pointer = arr1.length - 1 - count;
      int i = arr1[pointer];
      ++i;

      if (i > 9) {
        count++;
        arr1[pointer] = 0;
      } else {
        arr1[pointer] = i;
        enoth = false;
      }

    } while (enoth);

    return arr1;
  }

  private void print(int[] arr) {
    for (int i : arr) {
      System.out.println(i);
    }
  }
}
