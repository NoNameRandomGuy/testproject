package LC;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class RotateArray {

  @Test
  void test() {
    int[] x0 = {1, 2, 3, 4, 5, 6, 7};
    int[] x1 = {-1, -100, 3, 99};

    print(myRotate(x0.clone(), 1));
    print(myRotate(x0.clone(), 2));
    print(myRotate(x0.clone(), 3));
    System.out.println("=======");
    print(myRotate(x1.clone(), 1));
    print(myRotate(x1.clone(), 2));

    assertArrayEquals(myRotate(x0.clone(), 1), new int[] {7, 1, 2, 3, 4, 5, 6});
    assertArrayEquals(myRotate(x0.clone(), 2), new int[] {6, 7, 1, 2, 3, 4, 5});
    assertArrayEquals(myRotate(x0.clone(), 3), new int[] {5, 6, 7, 1, 2, 3, 4});
    assertArrayEquals(myRotate(x0.clone(), 4), new int[] {4, 5, 6, 7, 1, 2, 3});

    assertArrayEquals(myRotate(x1.clone(), 1), new int[] {99, -1, -100, 3});
    assertArrayEquals(myRotate(x1.clone(), 2), new int[] {3, 99, -1, -100});
  }

  public int[] myRotate(int[] nums, int k) {
    int x = k % nums.length;
    swap(nums, 0, nums.length - 1);
    swap(nums, 0, x - 1);
    swap(nums, x, nums.length - 1);

    return nums;
  }

  private void swap(int[] nums, int start, int end) {
    while (start < end) {
      int temp = nums[start];
      nums[start] = nums[end];
      nums[end] = temp;
      start++;
      end--;
    }
  }

  public void print(int[] prices) {
    for (int i : prices) {
      System.out.print(i + " ");
    }
    System.out.println();
  }

  @Test
  void tsts() {
    System.out.println(1 % 7);
  }

  public int[] myRotate1(int[] nums, int k) {
    int[] res = new int[nums.length];
    for (int i = 0; i < nums.length; i++) {
      int x = k % nums.length;
      int i1 = nums.length - x + i;
      int temp = i1 < nums.length ? i1 : i - x;
      res[i] = nums[temp];
    }
    return res;
  }
}
