package LC.IntersectionOfTwoArrays2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

class IntersectionOfTwoArrays2_2 {

  @Test
  void test() {
    int[] nums1 = {4, 9, 5, 9};
    int[] nums2 = {9, 4, 9, 8, 4};

    int[] x = find(nums1, nums2);
    print(x);
  }

  private int[] find(int[] arr1, int[] arr2) {
    Map<Integer, Integer> map = toMap(arr1);
    List<Integer> res = new ArrayList<>();
    for (int i : arr2) {
      Integer element = map.get(i);
      if (element != null && element > 0) {
        res.add(i);
        map.put(i, --element);
      }
    }
    return res.stream().mapToInt(Integer::intValue).toArray();

  }

  private Map<Integer, Integer> toMap(int[] arr) {
    HashMap<Integer, Integer> map = new HashMap<>();
    for (int i : arr) {
      Integer unit = map.get(i);
      if (unit == null) {
        map.put(i, 1);
      } else {
        map.put(i, ++unit);
      }
    }
    return map;
  }

  private void print(int[] arr) {
    for (int i : arr) {
      System.out.println(i);
    }
  }
}
