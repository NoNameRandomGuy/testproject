package LC.IntersectionOfTwoArrays2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

class IntersectionOfTwoArrays2 {

  @Test
  void test() {
    int[] nums1 = {4, 9, 5, 9,11};
    int[] nums2 = {9, 4, 9, 8, 4, 22};

    int[] x = find(nums1, nums2);
    print(x);
  }

  private int[] find(int[] smallSorted, int[] bigSorted) {
    Arrays.sort(smallSorted);
    Arrays.sort(bigSorted);
    ArrayList<Integer> res = new ArrayList<>();
    int f = 0;

    for (int i = 0; i < bigSorted.length && f < smallSorted.length; i++) {
      int v1 = bigSorted[i];
      while (f < smallSorted.length) {
        int v2 = smallSorted[f];
        if (v1 == v2) {
          res.add(v1);
          ++f;
          break;
        } else if (v1 < v2) {
          break;
        } else {
          ++f;
        }
      }
    }
    return res.stream().mapToInt(Integer::intValue).toArray();
  }


  private void print(int[] arr) {
    for (int i : arr) {
      System.out.print(i + " ");
    }
    System.out.println();
  }
}
