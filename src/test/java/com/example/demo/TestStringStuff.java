package com.example.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.paukov.combinatorics3.Generator;
import org.paukov.combinatorics3.IGenerator;

public class TestStringStuff {

  List<String> aylih = List.of("aylih", "aylh");
  List<String> data$eb = List.of("data$", "d$");
  List<String> wtf = List.of("wtf", "Wtf");

  Set<List<String>> unique = Set.of(aylih, data$eb, wtf);

  Set<List<String>> mustHave = Set.of(data$eb);

//  List<String> all = List.of("ntp", "@");
  List<String> all = List.of("1", "2", "3", "4", "5", "6", "7", "8");

  String input = "xeyywzyzyxw";

  @Test
  void contextLoads() {
    List<String> arr = new ArrayList<>();
    arr.addAll(all);
//    arr.addAll(aylih);
//    arr.addAll(data$eb);
//    arr.addAll(wtf);

    List<String> generate = generateInRenge(arr, 3, 4);
//    generate.forEach(System.out::println);
  }

  private List<String> generateInRenge(List<String> arr, int from, int to) {
    List<String> res = new ArrayList<>();
    for (int i = from; i <= to; i++) {
      res.addAll(generate(arr, i));
    }
    return res;
  }

  private List<String> generate(List<String> arr, int len) {
    IGenerator<List<String>> multi = Generator.combination(arr).multi(len);
    return multi.stream()
        .peek(System.out::println)
        .filter(this::isUnike)
        .map(strings -> String.join("", strings))
        .collect(Collectors.toList());
  }

  boolean isUnike(List<String> list) {
    Set<String> strings = new HashSet<>(list);
    return strings.size() == list.size() && containMustHave(strings) && checkAllPares(strings);
  }

  private boolean containMustHave(Set<String> strings) {
    for (List<String> ms : mustHave) {
      for (String s : ms) {
        if (strings.contains(s)) return true;
      }
    }
    return false;
  }

  boolean checkAllPares(Set<String> strings) {
    AtomicInteger cont = new AtomicInteger();
    for (List<String> ex : unique) {
      ex.forEach(
              s -> {
                if (strings.contains(s)) {
                  cont.incrementAndGet();
                }
              });
      if (cont.get() > 1) return false;
      cont.set(0);
    }
    return true;
  }
}
