package com.example.demo;

import java.util.List;
import org.junit.jupiter.api.Test;

// @SpringBootTest
class DemoApplicationTests {

  /*
  Даны два целочисленных списка. Нужно реализовать метод, который показывает,
  является ли второй список подсписком* первого.

      list = [5, 7, 8, 11], seq = [7, 11] => true
  list = [5, 7, 8, 11], seq = [5, 7, 8] => true
  list = [5, 7, 8, 11], seq = [5, 7, 11] => true
  list = [5, 7, 8, 11], seq = [7, 12] => false
  list = [5, 7, 8, 11], seq = [8, 5] => false
      */

  @Test
  void contextLoads() {
    System.out.println(isSubsequence(List.of(5, 7, 8, 11), List.of(7, 11)));
    System.out.println(isSubsequence(List.of(5, 7, 8, 11), List.of(5, 7, 8)));
    System.out.println(isSubsequence(List.of(5, 7, 8, 11), List.of(5, 7, 11)));
    System.out.println(isSubsequence(List.of(5, 7, 8, 11), List.of(7, 12)));
    System.out.println(isSubsequence(List.of(5, 7, 8, 11), List.of(8, 5)));
    System.out.println(isSubsequence(List.of(5, 7, 8, 11), List.of(8, 7)));
  }

  private boolean isSubsequence(List<Integer> list, List<Integer> toFind) {
    int position = 0;


    for (Integer i : list) {
      if (position < toFind.size() && i.equals(toFind.get(position))) {
        position++;
      }
    }
    return toFind.size() == position;
  }
}
