FROM openjdk:11
ADD target/*.jar opt/app.jar
WORKDIR opt/
ENTRYPOINT ["java", "-jar", "app.jar"]

ENV TZ=Europe/Moscow

EXPOSE 8080

